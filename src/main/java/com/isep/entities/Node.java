package com.isep.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Node {
    private String id;      //Id of the node
    private double lat;     //Latitude of the node
    private double lon;     //Longitude of the node
}
