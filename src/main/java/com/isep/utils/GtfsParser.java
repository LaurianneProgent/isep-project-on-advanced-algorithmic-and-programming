package com.isep.utils;

import com.isep.entities.Graph;
import com.isep.entities.Node;
import lombok.AllArgsConstructor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@AllArgsConstructor
public class GtfsParser {
    private String path;

    /**
     * Creates a graph filled with information taken from our GTFS files
     * @param isWeighted
     * @return
     */
    public Graph gtfsToGraph(boolean isWeighted) {
        Graph graph = new Graph();
        gtfsStopsToNodes(graph);
        gtfsNetworkToEdges(graph, isWeighted);
        return graph;
    }

    /**
     * Add to the graph its nodes with the stops.txt file
     * @param graph
     */
    private void gtfsStopsToNodes(Graph graph) {
        File file = new File(this.path + "stops.txt");
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] splitLine = line.split(",");
                Node newNode = new Node(splitLine[0], Double.parseDouble(splitLine[4]), Double.parseDouble(splitLine[5]));
                graph.addNode(newNode);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Add to the graph its edges with the network.txt file
     * @param graph
     * @param isWeighted
     */
    private void gtfsNetworkToEdges(Graph graph, boolean isWeighted) {
        File networkFile = new File(this.path + "network.txt");
        double weight = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(networkFile))) {
            String line;
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] splitLine = line.split(",");
                Node from = graph.findNodeById(splitLine[0]);
                Node to = graph.findNodeById(splitLine[1]);
                if (isWeighted) {
                    weight = weightCalculation(from, to);
                }
                graph.addEdge(from, to, weight);
                graph.addEdge(to, from, weight);
                graph.setM(graph.getM() + 1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Return the weight of the edge between the two input nodes
     * @param from
     * @param to
     * @return
     */
    private double weightCalculation(Node from, Node to) {
        double dx = to.getLon() - from.getLon();
        double dy = to.getLat() - from.getLat();
        return Math.sqrt((Math.pow(dx, 2)) + (Math.pow(dy, 2)));
    }
}
