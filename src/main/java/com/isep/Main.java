package com.isep;

import com.isep.entities.Graph;
import com.isep.entities.Node;
import com.isep.utils.BFSShortestPaths;
import com.isep.utils.Dijkstra;
import com.isep.utils.GtfsParser;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Main {

    public static void main(String[] args) {
        // Load resources
        GtfsParser gtfsParser = new GtfsParser("src/main/resources/");

        // Unweighted graph
        Graph graph = gtfsParser.gtfsToGraph(false);
        System.out.println("\n========== UNWEIGHTED GRAPH ==========\n\n" +
                "The graph :\n" +
                graph.toString() + "\n");

        // BFS shortest path
        BFSShortestPaths bfsSPs = new BFSShortestPaths();
        Node start = graph.findNodeById("M03");
        Node target = graph.findNodeById("M27");
        bfsSPs.bfs(graph, start);
        System.out.println("========== BFS SHORTEST PATH ==========\n\n" +
                "The BFS of the unweighted graph allows use to find the shortest path between " + start.getId() + " and " + target.getId() + ":\n");
        bfsSPs.printShortestPath(bfsSPs.getShortestPath(target));
        System.out.println("\n");

        // Weighted graph
        Graph weightedGraph = gtfsParser.gtfsToGraph(true);
        System.out.println("========== WEIGHTED GRAPH ==========\n\n" +
                "The weighted graph :\n" +
                weightedGraph.toString() + "\n");

        // Dijkstra shortest path
        Dijkstra dijkstra = new Dijkstra();
        Node weightedStart = weightedGraph.findNodeById("M13");
        Node weightedTarget = weightedGraph.findNodeById("M21");
        dijkstra.DikstraSP(weightedGraph, weightedStart);
        System.out.println("========== DIJKSTRA SHORTEST PATH ==========\n\n" +
                "The Dijkstra of the weighted graph allows use to find the shortest path between " + weightedStart.getId() + " and " + weightedTarget.getId() + ":\n");
        dijkstra.printShortestPath(dijkstra.getShortestPath(weightedTarget));
        System.out.println("\nThe distance between " + weightedStart.getId() + " and " + weightedTarget.getId() + " is " + dijkstra.distTo(weightedTarget));
        System.out.println("\n");

        // Clustering
        int mawClusters = 3;
        System.out.println("========== CLUSTERING ==========\n\n" +
                "We want " + mawClusters + " clusters :\n");
        CopyOnWriteArrayList<List<Node>> clusters = graph.makeCluster(mawClusters, false);
        for (List<Node> cluster : clusters) {
            System.out.print("CLUSTER : ");
            for (Node node : cluster) {
                System.out.print(node.getId() + " ");
            }
            System.out.println("\nSize = " + cluster.size());
        }
        System.out.println("NB OF CLUSTERS (TOTAL) = " + clusters.size());
    }
}
